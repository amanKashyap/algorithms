package searchingAlgorithm;

/*
 * Binary Search Has been implemented using Divide and Conquer technique,
 * Pre-Requisite:
 * 1. Array/Collection should be sorted.
 * 2. Collection should allow Random Access i.e. Indexing.
 */
public class BinarySearchExample {

	/*
	 * Below method is the Logic to search a Key using Binary Search Algorithm in an array.
	 * Given a Array and SearchKey,
	 * Return the index at which the searchKey is found.
	 * If not found return -1.
	 */
	public int searchBinaryAlgo(int[] arr, int searchKey) {
		/*
		 * Start and End Index of the passed array.
		 * Used to calculate middle Index.
		 */
		int startIndex = 0;
		int endIndex = arr.length - 1;
		
		while(startIndex <= endIndex) {
			int middleIndex = (startIndex + endIndex)/2;
			if(searchKey == arr[middleIndex]) {
				System.out.println("Search Key Found !!!");
				return middleIndex;
			} else if(searchKey < arr[middleIndex]) {
				/*
				 * As SearchKey < arr[middleIndex].
				 * Search in sub-array to the left of the middleIndex.
				 * Update the endIndex = middleIndex - 1.
				 */
				endIndex = middleIndex-1;
			} else {
				/*
				 * As SearchKey > arr[middleIndex].
				 * Search in sub-array to the right of the middleIndex.
				 * Update the startIndex = middleIndex + 1.
				 */
				startIndex = middleIndex + 1;
			}
		}
		
		return -1;
	}
	
	public static void main(String[] args) {
		BinarySearchExample binarySearchDemo = new BinarySearchExample();
		int[] arr = {1,2,3,4,5,6,7};
		int searchKey = 5;
		int index = binarySearchDemo.searchBinaryAlgo(arr, searchKey);
		if(index != -1) {
			System.out.println("Search Key : " + searchKey + " found at index : " + index);
		} else {
			System.out.println("Search Key Not found!!!");
		}
	}

}
