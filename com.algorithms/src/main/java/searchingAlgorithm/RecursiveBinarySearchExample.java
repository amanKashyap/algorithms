package searchingAlgorithm;

/*
 * Binary Search Has been implemented using Recursion,
 * Pre-Requisite:
 * 1. Array/Collection should be sorted.
 * 2. Collection should allow Random Access i.e. Indexing.
 */
public class RecursiveBinarySearchExample {

	public int recursiveBinarySearch(int[] sortedArr, int startIndex, int endIndex, int searchKey) {
		if (startIndex <= endIndex) {
			int middleIndex = (startIndex + endIndex) / 2;
			if (searchKey < sortedArr[middleIndex]) {
				return recursiveBinarySearch(sortedArr, startIndex, middleIndex - 1, searchKey);
			} else if (searchKey > sortedArr[middleIndex]) {
				return recursiveBinarySearch(sortedArr, middleIndex + 1, endIndex, searchKey);
			} else {
				System.out.println("Search Key Found !!");
				return middleIndex;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		RecursiveBinarySearchExample binarySearchDemo = new RecursiveBinarySearchExample();
		int[] arr = { 1, 2, 3, 4, 5, 6, 7 };
		int searchKey = 5;
		int index = binarySearchDemo.recursiveBinarySearch(arr, 0, arr.length - 1, searchKey);
		if (index != -1) {
			System.out.println("Search Key : " + searchKey + " found at index : " + index);
		} else {
			System.out.println("Search Key Not found!!!");
		}
	}

}
