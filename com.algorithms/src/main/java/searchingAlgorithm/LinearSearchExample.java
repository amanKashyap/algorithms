package searchingAlgorithm;

public class LinearSearchExample {

	/*
	 * Below method is the Logic to search a Key linearly in an array.
	 * Given a Array and SearchKey,
	 * Return the index at which the searchKey is found.
	 * If not found return -1.
	 */
	public int searchLinearly(int[] arr, int searchKey) {
		int lengthOfArray = arr.length;

		for (int i = 0; i < lengthOfArray; i++) {
			if (searchKey == arr[i]) {
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		LinearSearchExample linearSearchDemo = new LinearSearchExample();
		int[] arr = {1,2,3,4,5,6,7};
		int searchKey = 5;
		int index = linearSearchDemo.searchLinearly(arr, searchKey);
		if(index != -1) {
			System.out.println("Search Key : " + searchKey + " found at index : " + index);
		} else {
			System.out.println("Search Key Not found!!!");
		}
	}

}
