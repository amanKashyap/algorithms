package kadaneAlgorithm;

/*
 * The below mentioned code also works for an array containing just Negative Numbers.
 * Or for an array having More negative Numbers than the positive Numbers.
 */
public class LargestSumContiguousSubarrayAllNegative {

	
	public static int getLargestSum(int array[]) {
		int max_ended_here = 0;
		int max_so_far = array[0];
		
		/*
		 * 1. max_ended_here = Math.max(array[i], max_ended_here+array[i]);
		 * --> Takes the max value out of array[i] and max_ended_here+array[i]
		 */
		for(int i = 0; i< array.length; i++) {
			max_ended_here = Math.max(array[i], max_ended_here+array[i]);
			max_so_far = Math.max(max_so_far, max_ended_here);
		}
		return max_so_far;
	}
	
	public static void main(String[] args) {
		 System.out.println("Array with both Positive and Negative Values");
		 int [] array = {-2, -3, 4, -1, -2, 1, 5, -3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSum(array)); 
		 
		 System.out.println("Array with Only Positive values");
		 int [] array1 = {2, 3, 4, 1, 2, 1, 5, 3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSum(array1));
		 
		 System.out.println("Array with just Negative Values");
		 int [] array2 = {-2, -3, -4, -1, -2, -1, -5, -3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSum(array2)); 
	}
}
