package kadaneAlgorithm;

/*
 * The below mentioned code doesn't work for an array containing just Negative Numbers.
 * Or for an array having More negative Numbers than the positive Numbers.
 */
public class LargestSumContiguousSubarray {

	/*
	 * Logic is to Iterate the Array, one element at a time.
	 * Take the continuous sum of the array in max_ending_here.
	 * Comparing the max_so_far and max_ending_here and moving the highest value into max_so_far.
	 */
	public static int getLargestSubarraySum(int array[]) {
		int max_so_far = 0;
		int max_ending_here = 0;

		for (int i = 0; i < array.length; i++) {
			max_ending_here = max_ending_here + array[i];

			if (max_ending_here < 0) {
				max_ending_here = 0;
			}

			if (max_so_far < max_ending_here) {
				max_so_far = max_ending_here;
			}
		}
		
		return max_so_far;
	}
	
	/*
	 * Above Method can be Optimized 
	 * if we compare max_so_far with max_ending_here only if max_ending_here is greater than 0.
	 */
	public static int getLargestSubarraySumOptimized(int array[]) {
		int max_so_far = 0;
		int max_ending_here = 0;

		for (int i = 0; i < array.length; i++) {
			max_ending_here = max_ending_here + array[i];

			if (max_ending_here < 0) {
				max_ending_here = 0;
			} else if (max_so_far < max_ending_here) {
				max_so_far = max_ending_here;
			}
		}
		
		return max_so_far;
	}


	public static void main(String[] args) {
		System.out.println("Array with both Positive and Negative Values");
		 int [] array = {-2, -3, 4, -1, -2, 1, 5, -3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySum(array)); 
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySumOptimized(array)); 
		 
		 System.out.println("Array with Only Positive values");
		 int [] array1 = {2, 3, 4, 1, 2, 1, 5, 3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySum(array1));
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySumOptimized(array1));
		 
		 System.out.println("Array with just Negative Values");
		 int [] array2 = {-2, -3, -4, -1, -2, -1, -5, -3}; 
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySum(array2)); 
		 System.out.println("Largest SubArray Sum :" + getLargestSubarraySumOptimized(array2)); 
	}

}
