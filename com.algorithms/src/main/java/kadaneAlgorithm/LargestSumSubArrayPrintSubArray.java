package kadaneAlgorithm;

public class LargestSumSubArrayPrintSubArray {
	
	public static void maxSubArraySum(int array[]) {
		/*
		 * 1. start and end index would hold the starting and ending index of the Subarray.
		 * 2. max_ending_here would hold the intermediate sum of the array.
		 * 3. max_so_far would hold the largest sum of the subarray.
		 * 4. s would hold the intermediate start index of the Subarray.
		 */
		int start = 0;
		int end = 0;
		int max_ending_here = 0;
		int max_so_far = array[0];
		int s = 0;
		
		for(int i =0; i < array.length ; i++) {
			max_ending_here = max_ending_here + array[i];
			
			if(max_so_far< max_ending_here) {
				max_so_far = max_ending_here;
				start = s;
				end = i;
			} 
			
			if (max_ending_here < 0) {
				max_ending_here = 0;
				s = i+1;
			}
		}
		
		System.out.println("Largest Sum of SubArray : "+ max_so_far);
		System.out.println("Starting Index : "+ start);
		System.out.println("Ending Index : "+end);
	}
	
	public static void main(String[] args) {
		
		System.out.println("Array with just Positive and Negative Values");
		int[] array = { -2, -3, 4, -1, -2, 1, 5, -3 };
		maxSubArraySum(array);
		
		System.out.println("Array with just Positive Values");
		int[] array1 = { 2, 3, 4, 1, 2, 1, 5, 3 };
		maxSubArraySum(array1);
		
		System.out.println("Array with just Negative Values");
		int[] array2 = { -2, -3, -4, -1, -2, -1, -5, -3 };
		maxSubArraySum(array2);
	}
}
