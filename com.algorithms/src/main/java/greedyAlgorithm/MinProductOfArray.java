package greedyAlgorithm;

public class MinProductOfArray {
	/*
	 * @Arg - int arr[] - containing all the elements in the array.
	 * 		  int n - Total Number of elements in array
	 * @Return - int - minimun product of a subset of the passed array.
	 */
	static int  minProductSubSet(int arr[], int n) {
		
		//Negative number, Min Value
		int negMax = Integer.MIN_VALUE;
		//Positive Number, Min Value
		int posMin = Integer.MAX_VALUE;
		//Number of Zero's in the array.
		int count_zero = 0;
		//Number of Negative Number in the array.
		int count_neg = 0;
		//Product of Array.
		int product = 1;
		
		/*
		 * If array consist of a Single element.
		 * There would only be one subset of that array, consisting of the single element.
		 */
		if(n == 1) {
			return arr[0];
		}
		
		/*
		 * Iterating the array, One element at a time.
		 */
		for(int i= 0; i < n ;i++) {
			
			/*
			 * If the Element if Zero,
			 * Increase the count of count_zero and continue iterating the next element.
			 */
			if(arr[i] == 0) {
				count_zero++;
				continue;
			}
			
			/*
			 * If the element is a Negative Number.
			 * Increase the count of count_neg,
			 * Find the max negative value between negMax and arr[i]
			 */
			if(arr[i] < 0) {
				count_neg++;
				negMax = Math.max(negMax, arr[i]);
			}
			
			/*
			 * If the element is a Positive Number,
			 * Find out the Min Positive number from the array.
			 */
			if(arr[i] > 0 && arr[i] < posMin) {
				posMin = arr[i];
			}
			
			product = product*arr[i];
		}
		
		/*
		 * 1. If Either all the elements in the array are Zero
		 * 2. Else, If there are no Negative Number and One or More Zero Elements.
		 * --> Return 0
		 */
		if (count_zero == n || (count_neg == 0 && count_zero > 0)) {
			return 0;
		}
		
		/*
		 * 1. If there are No Zero Elements.
		 * 2. There are No Negative Elements.
		 * --> Return the Least Positive Number from the array.
		 * 	   (Single Element would make the smallest Subset, and 
		 * 		Min valued positive number would make the Min Product of any subset in Array)
		 */
		if(count_neg == 0) {
			return posMin;
		}
		
		/*
		 * 1. If there are even Number of Negative Numbers.
		 * 2. There are some Negative Numbers present in the array.
		 */
		if(count_neg%2 == 0 && count_neg != 0) {
			/*
			 * Result would be product/negMax.
			 * i.e. Considering on the Smallest Negative Number and all the positive Numbers.
			 * Eg: [-10, 2, 4, -5]
			 * Min Product of a Subset is ( -10 * 2 * 4) ==> -80
			 */
			product = product/negMax;
		}
		return product;
	}
	
	public static void main(String[] args) {
		int a[] = { -1, -1, -2, 4, 3 }; 
		int a_n = 5;
		
		int b[] = { -1, 0 }; 
		int b_n = 2;
		
		int c[] = { 0, 0, 0 }; 
		int c_n = 3;
		
		int d[] = { 1, 5, 6, 4, 3 }; 
		int d_n = 5;
		
		int e[] = { 1, -5, -6, 4, 3 }; 
		int e_n = 5;
		
		System.out.println("a :" + minProductSubSet(a, a_n));
		System.out.println("b :" + minProductSubSet(b, b_n));
		System.out.println("c :" + minProductSubSet(c, c_n));
		System.out.println("d :" + minProductSubSet(d, d_n));
		System.out.println("e :" + minProductSubSet(e, e_n));
	}
}
