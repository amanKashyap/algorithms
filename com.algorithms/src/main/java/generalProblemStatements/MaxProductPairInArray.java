package generalProblemStatements;

/*
 * Given a array, Find out the max product of any particular pair in the array.
 * Pair can be of Continuous elements in array or non-continuous.
 */
public class MaxProductPairInArray {
	
	/*
	 * First Approach is to consider every pair in the array.
	 * Check its product and Find the pair with max product.
	 * @arg - int array containing the array elements, 
	 * 		  n - total number of elements.
	 * Time Complexity : O(n2)
	 * --> If we sort the array and fetch out the largest product of two elements.
	 * Time Complexity : O(nlogn)
	 */
	public static void maxProducts(int arr[], int n) {
		//If No Pair Exists
		if (n < 2) {
			System.out.println("No Pair Exists.");
			return;
		}
		
		// Initialize max product pair 
        int a = arr[0], b = arr[1]; 
        
       //If Only One Pair Exists.
  		if (n == 2) {
  			System.out.println("Only One Pair Exists");
  			System.out.println("Max Product of Pair: " + a*b);
  			System.out.println("Elements of Pair : " + a + " : " + b);
  			return;
  		}
      		
		/*
		 * Traverse Every Pair of the Array.
		 * Keep Track of the Max Product
		 */
		
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (arr[i] * arr[j] > a * b) {
					a = arr[i];
					b = arr[j];
				}
			}
		}
		
		System.out.println("Max Product of Pair: " + a*b);
		System.out.println("Elements of Pair : " + a + " : " + b);
	}
	
	
	/*
	 * Approach Two:
	 * Better Approach would be to solve this problem in a Single traversal of an array.
	 * We need to track below properties:
	 * 1. Largest Positive Number.
	 * 2. Second Largest Positive Number
	 * 3. Absolute of Largest Negative Number
	 * 4. Absolute of Second Largest Negative Number.
	 * 
	 * Then in the end we can compare these two products 
	 * i.e. Which combination is more and Find out the Max Product of a Pair in an Array.
	 * 
	 * Time Complexity - O(n)
	 */
	public static void maxProductsOptimized(int arr[], int n) {
		//If No Pair Exists.
		if (n < 2) {
			System.out.println("No Pair Exists.");
			return;
		}

		// If Only One Pair Exists.
		if (n == 2) {
			System.out.println("Only One Pair Exists");
			System.out.println("Max Product of Pair: " + arr[0] * arr[1]);
			System.out.println("Elements of Pair : " + arr[0] + " : " + arr[1]);
			return;
		}
		
		int maxPos = Integer.MIN_VALUE;
		int secondMaxPos  = Integer.MIN_VALUE;
		int maxNeg = Integer.MIN_VALUE;
		int secondMaxNeg = Integer.MIN_VALUE;
		
		for(int i= 0 ; i<n ; i++) {
			/*
			 * Logic To find Max Positive and Second Max Positive Number.
			 */
			if(arr[i] > maxPos) {
				secondMaxPos = maxPos;
				maxPos = arr[i];
			} else if(arr[i] > secondMaxPos) {
				secondMaxPos = arr[i]; 
			}
			
			
			if (arr[i] < 0 && Math.abs(arr[i]) > Math.abs(maxNeg)) {
				secondMaxNeg = maxNeg;
				maxNeg = arr[i];
			} else if (arr[i] < 0 && Math.abs(arr[i]) > Math.abs(secondMaxNeg)) {
				secondMaxNeg = arr[i];
			}
		}
		
		
		if((maxNeg*secondMaxNeg) > (maxPos*secondMaxPos)) {
			System.out.println("Max Product of Pair: " + maxNeg * secondMaxNeg);
			System.out.println("Elements of Pair : " + maxNeg + " : " + secondMaxNeg);
		} else {
			System.out.println("Max Product of Pair: " + maxPos * secondMaxPos);
			System.out.println("Elements of Pair : " + maxPos + " : " + secondMaxPos);
		}
	}
	
	public static void main(String[] args) {
		int arr[] = {1, 4, 3, 6, 7, 0}; 
        int n = arr.length; 
        maxProducts(arr, n);
        System.out.println("**************************************");
        maxProductsOptimized(arr,n);
	}
}

