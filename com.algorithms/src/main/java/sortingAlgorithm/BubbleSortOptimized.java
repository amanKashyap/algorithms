package sortingAlgorithm;

public class BubbleSortOptimized {

	public int[] bubbleSort(int[] list) {
		
		int i = 0;
		int j = 0;
		
		int temp = 0;
		/*
		 * We can Optimize the Bubble Sort by introducing a flag.
		 * We can Make the flag as true. Whenever a swap takes place in inner loop.
		 * Logic is that: If for a particular iteration no swapping takes place then
		 * it means that array has been sorted.
		 * We can jump out of for loop, instead of executing all iterations.
		 */

		for (i = 0; i < list.length - 1; i++) {
			boolean flag = false;
			for (j = 0; j < (list.length - 1) - i; j++) {
				if (list[j] > list[j + 1]) {
					temp = list[j];
					list[j] = list[j + 1];
					list[j + 1] = temp;
					flag = true;
				}
			}
			
			if(!flag) {
				System.out.println("Array is Sorted in iterations : " + i);
				break;
			}
		}

		return list;
	}
	
	
	public static void main(String[] args) {
		BubbleSortOptimized bubbleSort = new BubbleSortOptimized();
		System.out.println("Unsorted Array");
		int[] arr = { 1, 2, 3, 4, 6, 5 };
		System.out.print("{");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + "  ");
		}
		System.out.print("}");
		System.out.println();
		System.out.println("*********************************************");
		int[] sortedArr = bubbleSort.bubbleSort(arr);
		System.out.println("Sorted Array");
		System.out.print("{");
		for (int i = 0; i < sortedArr.length; i++) {
			System.out.print(sortedArr[i] + "  ");
		}
		System.out.print("}");
	}
}
