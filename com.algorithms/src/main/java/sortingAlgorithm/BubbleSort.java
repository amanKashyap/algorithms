package sortingAlgorithm;

/*
 * In-place Sorting Algorithm.
 * O(N^2) -- Big O Notation
 * Compares Adjacent items.
 * With Each Iteration, The Largest Element out of the lot is moved to the end.
 * Not much efficient sorting algo as it uses Nested loops.
 */
public class BubbleSort {

	
	public int[] bubbleSort(int[] list) {
		/*
		 * i --> outer loop; range 0 - list.length-1 j --> inner loop; range 0 -
		 * (list.length-1)-i (As with each iteration of outer loop, One element would
		 * get sorted Which we dont need to consider while sorting the rest of elements)
		 * We are going to compare list[j] and list[j+1] elements.
		 */
		int i = 0;
		int j = 0;
		/*
		 * temp variable should be used while swapping the two elements.
		 */
		int temp = 0;

		for (i = 0; i < list.length - 1; i++) {
			for (j = 0; j < (list.length - 1) - i; j++) {
				if (list[j] > list[j + 1]) {
					temp = list[j];
					list[j] = list[j + 1];
					list[j + 1] = temp;
				}
			}
		}

		return list;
	}
	
	public static void main(String[] args) {
		BubbleSort bubbleSort = new BubbleSort();
		System.out.println("Unsorted Array");
		int[] arr = { 5, 8, 1, 6, 9, 2 };
		System.out.print("{");
		for(int i= 0; i<arr.length ; i++) {
			System.out.print(arr[i] + "  ");
		}
		System.out.print("}");
		System.out.println();
		System.out.println("*********************************************");
		int[] sortedArr = bubbleSort.bubbleSort(arr);
		System.out.println("Sorted Array");
		System.out.print("{");
		for(int i= 0; i<sortedArr.length ; i++) {
			System.out.print(sortedArr[i] + "  ");
		}
		System.out.print("}");
	}
}
