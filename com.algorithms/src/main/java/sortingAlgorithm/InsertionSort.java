package sortingAlgorithm;

/*
 * In-place Sorting Algorithm.
 * O(N^2) -- Big O Notation
 * Sorting Starts from the left,
 * First Element is considered sorted.
 * Not much efficient sorting algo as it uses Nested loops.
 */
public class InsertionSort {
	public int[] insertionSort(int[] list) {
		/*
		 * i --> outer loop; range 1 - list.length-1 
		 * j --> inner loop; range i-1 to 0.
		 * key --> list[i]
		 */
		int i = 0;
		int j = 0;

		int key, temp;

		for (i = 1; i < list.length; i++) {
			key = list[i];
			j = i - 1;
			while (j >= 0 && key < list[j]) {

				// Swap the elements.
				temp = list[j];
				list[j] = list[j + 1];
				list[j + 1] = temp;

				j--;
			}
		}

		return list;
	}
	
	public static void main(String[] args) {
		InsertionSort insertionSort = new InsertionSort();
		System.out.println("Unsorted Array");
		int[] arr = { 5, 8, 1, 6, 9, 2 };
		System.out.print("{");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + "  ");
		}
		System.out.print("}");
		System.out.println();
		System.out.println("*********************************************");
		int[] sortedArr = insertionSort.insertionSort(arr);
		System.out.println("Sorted Array");
		System.out.print("{");
		for (int i = 0; i < sortedArr.length; i++) {
			System.out.print(sortedArr[i] + "  ");
		}
		System.out.print("}");
	}
}
